package com.distributedsystems.lab4.repository;

import com.distributedsystems.lab4.entity.TagEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface TagRepository extends JpaRepository<TagEntity, BigInteger> {

    public Iterable<TagEntity> findByNodeId(BigInteger id);
}
