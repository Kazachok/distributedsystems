package com.distributedsystems.lab4.repository;

import com.distributedsystems.lab4.entity.NodeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.math.BigInteger;

@Repository
public interface NodeRepository extends JpaRepository<NodeEntity, BigInteger> {

    @Query(
            nativeQuery = true,
            value = "select * from nodes " +
                    "where earth_box(ll_to_earth(:lat, :lon), :radius) @> ll_to_earth(lat, lon) " +
                    "order by point(lon, lat) <@> point(:lon, :lat) desc"
    )
    Iterable<NodeEntity> findNearNodes(
            @Param("lat") Double lat,
            @Param("lon") Double lon,
            @Param("radius") Double radius
    );
}
