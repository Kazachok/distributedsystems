package com.distributedsystems.lab4.entity;

import com.distributedsystems.lab4.dto.TagDto;
import com.fasterxml.jackson.annotation.JsonIgnore;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.math.BigInteger;

@Entity
@Table(name = "tags")
public class TagEntity {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private BigInteger id;

    @Column(name = "k")
    private String key;

    @Column(name = "v")
    private String value;

    @ManyToOne
    @JoinColumn(name = "nodeid")
    @JsonIgnore
    private NodeEntity node;

    public TagEntity(@NotNull TagDto tagDto) {
        this.id = tagDto.getId();
        this.key = tagDto.getKey();
        this.value = tagDto.getValue();
    }

    public TagEntity() {}

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public NodeEntity getNode() {
        return node;
    }

    public void setNode(NodeEntity nodeId) {
        this.node = nodeId;
    }
}
