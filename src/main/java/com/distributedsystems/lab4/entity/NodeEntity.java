package com.distributedsystems.lab4.entity;

import com.distributedsystems.lab4.dto.NodeDto;
import org.jetbrains.annotations.NotNull;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "nodes")
public class NodeEntity {

    @Id
    @Column(name = "id")
    private BigInteger id;

    @Column(name = "lat")
    private Double lat;

    @Column(name = "lon")
    private Double lon;

    @Column(name = "username")
    private String user;

    @Column(name = "uid")
    private BigInteger uid;

    @Column(name = "visible")
    private Boolean visible;

    @Column(name = "versionvalue")
    private BigInteger version;

    @Column(name = "changeset")
    private BigInteger changeSet;

    @OneToMany(mappedBy = "node", fetch = FetchType.EAGER)
    private List<TagEntity> tags;

    public NodeEntity(@NotNull NodeDto nodeDto) {
        this.id = nodeDto.getId();
        this.lat = nodeDto.getLat();
        this.lon = nodeDto.getLon();
        this.user = nodeDto.getUser();
        this.uid = nodeDto.getUid();
        this.visible = nodeDto.getVisible();
        this.version = nodeDto.getVersion();
        this.changeSet = nodeDto.getChangeSet();
    }

    public NodeEntity() {}

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public BigInteger getUid() {
        return uid;
    }

    public void setUid(BigInteger uid) {
        this.uid = uid;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public BigInteger getVersion() {
        return version;
    }

    public void setVersion(BigInteger version) {
        this.version = version;
    }

    public BigInteger getChangeSet() {
        return changeSet;
    }

    public void setChangeSet(BigInteger changeSet) {
        this.changeSet = changeSet;
    }

    public List<TagEntity> getTags() {
        return tags;
    }

    public void setTags(List<TagEntity> tags) {
        this.tags = tags;
    }
}
