package com.distributedsystems.lab4.controller;

import com.distributedsystems.lab4.dto.NodeDto;
import com.distributedsystems.lab4.entity.NodeEntity;
import com.distributedsystems.lab4.service.NodeService;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;
import java.util.List;

@RestController
@RequestMapping("/node")
public class NodeController {

    private final NodeService nodeService;

    public NodeController(NodeService nodeService) {
        this.nodeService = nodeService;
    }

    @PostMapping
    public void save(@RequestBody NodeDto node) {
        nodeService.save(node);
    }

    @GetMapping("/{id}")
    public NodeDto get(@PathVariable BigInteger id) {
        return nodeService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable BigInteger id) {
        nodeService.delete(id);
    }

    @GetMapping("/near")
    public List<NodeEntity> getNearNodes(
            @RequestParam("lat") Double lat,
            @RequestParam("lon") Double lon,
            @RequestParam("radius") Double radius
    ) {
        return nodeService.getNearNodes(lat, lon, radius);
    }
}
