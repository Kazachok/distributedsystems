package com.distributedsystems.lab4.controller;

import com.distributedsystems.lab4.dto.TagDto;
import com.distributedsystems.lab4.service.TagService;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequestMapping("/tag")
public class TagController {

    private final TagService tagService;

    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @PostMapping
    public void save(@RequestBody TagDto tag) {
        tagService.save(tag);
    }

    @GetMapping("/{id}")
    public TagDto get(@PathVariable BigInteger id) {
        return tagService.get(id);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable BigInteger id) {
        tagService.delete(id);
    }
}
