package com.distributedsystems.lab4.service;

import com.distributedsystems.lab4.dto.TagDto;
import com.distributedsystems.lab4.entity.TagEntity;
import com.distributedsystems.lab4.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
public class TagService {

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public void save(TagDto tagDto) {
        tagRepository.save(new TagEntity(tagDto));
    }

    public TagDto get(BigInteger id) {
        return new TagDto(tagRepository.findById(id).get());
    }

    public void delete(BigInteger id) {
        tagRepository.deleteById(id);
    }
}
