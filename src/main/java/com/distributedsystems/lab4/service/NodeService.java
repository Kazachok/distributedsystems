package com.distributedsystems.lab4.service;

import com.distributedsystems.lab4.dto.NodeDto;
import com.distributedsystems.lab4.dto.TagDto;
import com.distributedsystems.lab4.entity.NodeEntity;
import com.distributedsystems.lab4.entity.TagEntity;
import com.distributedsystems.lab4.repository.NodeRepository;
import com.distributedsystems.lab4.repository.TagRepository;
import org.springframework.stereotype.Service;

import java.math.BigInteger;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class NodeService {

    private final NodeRepository nodeRepository;
    private final TagRepository tagRepository;

    public NodeService(NodeRepository nodeRepository, TagRepository tagRepository) {
        this.nodeRepository = nodeRepository;
        this.tagRepository = tagRepository;
    }

    public void save(NodeDto node) {
        nodeRepository.save(new NodeEntity(node));
        node.getTags().forEach(tagDto ->
                tagRepository.save(new TagEntity(tagDto))
        );
    }

    public NodeDto get(BigInteger id) {
        NodeEntity nodeEntity = nodeRepository.findById(id).get();
        List<TagDto> tagDtoList =
                StreamSupport.stream(tagRepository.findByNodeId(id).spliterator(), false)
                        .map(TagDto::new)
                        .collect(Collectors.toList());
        return new NodeDto(nodeEntity, tagDtoList);
    }

    public void delete(BigInteger id) {
        nodeRepository.deleteById(id);
    }

    public List<NodeEntity> getNearNodes(Double lat, Double lon, Double radius) {
        return StreamSupport.stream(nodeRepository.findNearNodes(lat, lon, radius).spliterator(), false)
                .collect(Collectors.toList());
    }
}
