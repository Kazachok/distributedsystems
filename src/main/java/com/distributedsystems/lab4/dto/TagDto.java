package com.distributedsystems.lab4.dto;

import com.distributedsystems.lab4.entity.TagEntity;
import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;

public class TagDto {

    private BigInteger id;

    private String key;

    private String value;

    public TagDto(BigInteger id, String key, String value) {
        this.id = id;
        this.key = key;
        this.value = value;
    }

    public TagDto(@NotNull TagEntity tagEntity) {
        this.id = tagEntity.getId();
        this.key = tagEntity.getKey();
        this.value = tagEntity.getValue();
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
