package com.distributedsystems.lab4.dto;

import com.distributedsystems.lab4.entity.NodeEntity;
import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;
import java.util.List;

public class NodeDto {

    private BigInteger id;

    private Double lat;

    private Double lon;

    private String user;

    private BigInteger uid;

    private Boolean visible;

    private BigInteger version;

    private BigInteger changeSet;

    private List<TagDto> tags;

    public NodeDto(BigInteger id, Double lat, Double lon, String user, BigInteger uid, Boolean visible, BigInteger version, BigInteger changeSet, List<TagDto> tags) {
        this.id = id;
        this.lat = lat;
        this.lon = lon;
        this.user = user;
        this.uid = uid;
        this.visible = visible;
        this.version = version;
        this.changeSet = changeSet;
        this.tags = tags;
    }

    public NodeDto(@NotNull NodeEntity nodeEntity, List<TagDto> tags) {
        this.id = nodeEntity.getId();
        this.lat = nodeEntity.getLat();
        this.lon = nodeEntity.getLat();
        this.user = nodeEntity.getUser();
        this.uid = nodeEntity.getUid();
        this.visible = nodeEntity.getVisible();
        this.version = nodeEntity.getVersion();
        this.changeSet = nodeEntity.getChangeSet();
        this.tags = tags;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public BigInteger getUid() {
        return uid;
    }

    public void setUid(BigInteger uid) {
        this.uid = uid;
    }

    public Boolean getVisible() {
        return visible;
    }

    public void setVisible(Boolean visible) {
        this.visible = visible;
    }

    public BigInteger getVersion() {
        return version;
    }

    public void setVersion(BigInteger version) {
        this.version = version;
    }

    public BigInteger getChangeSet() {
        return changeSet;
    }

    public void setChangeSet(BigInteger changeSet) {
        this.changeSet = changeSet;
    }

    public List<TagDto> getTags() {
        return tags;
    }

    public void setTags(List<TagDto> tags) {
        this.tags = tags;
    }
}
