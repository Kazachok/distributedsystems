package com.distributedsystems.lab3;

import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.Statement;

import static com.distributedsystems.lab3.Main3.logger;

public class DBManager {

    public void initTables(@NotNull Connection connection) {
        deleteTagTable(connection);
        deleteNodeTable(connection);
        createNodeTable(connection);
        createTagTable(connection);
    }

    public void createNodeTable(@NotNull Connection connection) {
        try {
            Statement statement = connection.createStatement();
            String sql = "create table Nodes " +
                    "(id bigserial primary key, " +
                    "lat numeric, " +
                    "lon numeric, " +
                    "userName text, " +
                    "uid bigint, " +
                    "visible boolean, " +
                    "versionValue bigint, " +
                    "changeSet bigint)";
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private void deleteNodeTable(@NotNull Connection connection) {
        try {
            Statement statement = connection.createStatement();
            String sql = "drop table if exists Nodes";
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void createTagTable(@NotNull Connection connection) {
        try {
            Statement statement = connection.createStatement();
            String sql = "create table Tags " +
                    "(id bigserial primary key, " +
                    "k text, " +
                    "v text, " +
                    "nodeId bigint references Nodes(id))";
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private void deleteTagTable(@NotNull Connection connection) {
        try {
            Statement statement = connection.createStatement();
            String sql = "drop table if exists Tags";
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }
}
