package com.distributedsystems.lab3;

import com.distributedsystems.lab1.Main;
import com.distributedsystems.lab3.dao.NodeDao;
import com.distributedsystems.lab3.dao.TagDao;
import com.distributedsystems.lab3.parser.ParserForBatch;
import com.distributedsystems.lab3.parser.ParserForPreparedStatement;
import com.distributedsystems.lab3.parser.ParserForStatement;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.nio.file.Files;
import java.nio.file.Path;

public class Main3 {

    public final static Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        Option fileOption = Option.builder("f")
                .hasArg(true)
                .longOpt("file")
                .build();
        Options options = new Options().addOption(fileOption);
        DBConnection connection = new DBConnection();
        DBManager dbManager = new DBManager();

        try {
            CommandLine commandLine = new DefaultParser().parse(options, args, true);
            BZip2CompressorInputStream inputStream = new BZip2CompressorInputStream(
                    new BufferedInputStream(
                            Files.newInputStream(
                                    Path.of(commandLine.getOptionValue("file"))
                            )
                    )
            );
            dbManager.initTables(connection.get());
            NodeDao nodeDao = new NodeDao(connection.get());
            TagDao tagDao = new TagDao(connection.get());
            ParserForStatement parserForStatement = new ParserForStatement();
            ParserForPreparedStatement parserForPreparedStatement = new ParserForPreparedStatement();
            ParserForBatch parserForBatch = new ParserForBatch();
            //long timeStatement = parserForStatement.parse(inputStream, nodeDao, tagDao);
            //System.out.println("Statement: 10000 nodes за " + timeStatement + " миллисекунд");
            //long timePreparedStatement = parserForPreparedStatement.parse(inputStream, nodeDao, tagDao);
            //System.out.println("PreparedStatement: 10000 nodes за " + timePreparedStatement + " миллисекунд");
            long timeBatch = parserForBatch.parse(inputStream, nodeDao, tagDao);
            System.out.println("Batch: 10000 nodes за " + timeBatch + " миллисекунд");
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            connection.close();
        }
    }
}
