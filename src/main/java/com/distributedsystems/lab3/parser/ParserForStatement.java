package com.distributedsystems.lab3.parser;

import com.distributedsystems.lab3.dao.NodeDao;
import com.distributedsystems.lab3.dao.TagDao;
import generated.Node;
import generated.Tag;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

public class ParserForStatement {

    private final QName nodeQName = new QName("node");

    public long parse(BZip2CompressorInputStream inputStream, NodeDao nodeDao, TagDao tagDao) throws JAXBException, XMLStreamException {
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
        JAXBContext context = JAXBContext.newInstance(Node.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        int counter = 0;
        long startTime = System.currentTimeMillis();
        long finishTime = System.currentTimeMillis();
        for (; reader.hasNext(); reader.next()) {
            if (counter >= 10000) {
                finishTime = System.currentTimeMillis() - startTime;
                break;
            }
            XMLEvent event = reader.peek();
            if (event.isStartElement()) {
                StartElement element = event.asStartElement();
                if (nodeQName.equals(element.getName())) {
                    counter++;
                    Node node = unmarshaller.unmarshal(reader, Node.class).getValue();
                    nodeDao.saveByStatement(node);
                    for (Tag tag : node.getTag()) {
                        tagDao.saveByStatement(tag, node.getId());
                    }
                }
            }
        }
        return finishTime;
    }
}