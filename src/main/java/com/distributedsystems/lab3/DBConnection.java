package com.distributedsystems.lab3;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static com.distributedsystems.lab3.Main3.logger;

public class DBConnection {

    private Connection connection;

    public DBConnection() {
        try {
            Class.forName("org.postgresql.Driver");
            String connectionUrl = "jdbc:postgresql://localhost:5432/postgres";
            String username = "postgres";
            String password = "postgres";
            connection = DriverManager.getConnection(connectionUrl, username, password);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public Connection get() {
        return connection;
    }

    public void close() {
        try {
            connection.close();
        } catch (SQLException e) {
            logger.error("", e);
        }
    }
}
