package com.distributedsystems.lab3.dao;

import generated.Tag;
import org.jetbrains.annotations.NotNull;

import java.math.BigInteger;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import static com.distributedsystems.lab3.Main3.logger;

public class TagDao {

    private final String PREPARED_STATEMENT = "insert into Tags(k, v, nodeId) values(?,?,?)";

    Connection connection;

    public TagDao(Connection connection) {
        this.connection = connection;
    }

    public void saveByStatement(Tag tag, BigInteger nodeId) {
        try {
            Statement statement = connection.createStatement();
            String sql = "insert into Tags(k, v, nodeId) values('" +
                    tag.getK() +
                    "', '" + tag.getV() +
                    "', " + nodeId +
                    ")";
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void saveByPreparedStatement(Tag tag, BigInteger nodeId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PREPARED_STATEMENT);
            setPreparedStatement(preparedStatement, tag, nodeId);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void saveByBatch(List<Tag> tags, BigInteger nodeId) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PREPARED_STATEMENT);
            for (Tag tag : tags) {
                setPreparedStatement(preparedStatement, tag, nodeId);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            preparedStatement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private void setPreparedStatement(@NotNull PreparedStatement preparedStatement, @NotNull Tag tag, BigInteger nodeId) {
        try {
            preparedStatement.setString(1, tag.getK());
            preparedStatement.setString(2, tag.getV());
            preparedStatement.setObject(3, nodeId);
        } catch (Exception e) {
            logger.error("", e);
        }
    }
}
