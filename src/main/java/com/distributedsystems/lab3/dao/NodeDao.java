package com.distributedsystems.lab3.dao;

import generated.Node;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.List;

import static com.distributedsystems.lab3.Main3.logger;

public class NodeDao {

    private final String PREPARED_STATEMENT = "insert into Nodes(id, lat, lon, userName, uid, visible, versionValue, changeSet) values(?,?,?,?,?,?,?,?)";

    Connection connection;

    public NodeDao(Connection connection) {
        this.connection = connection;
    }

    public void saveByStatement(Node node) {
        try {
            Statement statement = connection.createStatement();
            String sql = "insert into Nodes(id, lat, lon, userName, uid, visible, versionValue, changeSet) values(" +
                    node.getId() +
                    ", " + node.getLat() +
                    ", " + node.getLon() +
                    ", '" + node.getUser() +
                    "', " + node.getUid() +
                    ", " + node.isVisible() +
                    ", " + node.getVersion() +
                    ", " + node.getChangeset() +
                    ")";
            statement.executeUpdate(sql);
            statement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void saveByPreparedStatement(Node node) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PREPARED_STATEMENT);
            setPreparedStatement(preparedStatement, node);
            preparedStatement.execute();
            preparedStatement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    public void saveByBatch(List<Node> nodes) {
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(PREPARED_STATEMENT);
            for (Node node : nodes) {
                setPreparedStatement(preparedStatement, node);
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();
            preparedStatement.close();
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private void setPreparedStatement(@NotNull PreparedStatement preparedStatement, @NotNull Node node) {
        try {
            preparedStatement.setObject(1, node.getId());
            preparedStatement.setDouble(2, node.getLat());
            preparedStatement.setDouble(3, node.getLon());
            preparedStatement.setString(4, node.getUser());
            preparedStatement.setObject(5, node.getUid());
            preparedStatement.setObject(6, node.isVisible());
            preparedStatement.setObject(7, node.getVersion());
            preparedStatement.setObject(8, node.getChangeset());
        } catch (Exception e) {
            logger.error("", e);
        }
    }
}
