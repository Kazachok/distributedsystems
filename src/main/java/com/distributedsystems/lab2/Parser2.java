package com.distributedsystems.lab2;

import com.distributedsystems.lab1.ParseResult;
import generated.Node;
import generated.Tag;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jetbrains.annotations.NotNull;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.math.BigInteger;
import java.util.*;

public class Parser2 {

    private final QName nodeQName = new QName("node");

    public ParseResult parse(BZip2CompressorInputStream inputStream) throws JAXBException, XMLStreamException {
        Map<String, Set<String>> changes = new HashMap<>();
        Map<String, Integer> countTags = new HashMap<>();
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
        JAXBContext context = JAXBContext.newInstance(Node.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        for (; reader.hasNext(); reader.next()) {
            XMLEvent event = reader.peek();
            if (event.isStartElement()) {
                StartElement element = event.asStartElement();
                if (nodeQName.equals(element.getName())) {
                    Node node = unmarshaller.unmarshal(reader, Node.class).getValue();
                    handleNode(node, changes);
                    List<Tag> tagList = node.getTag();
                    if (tagList != null) {
                        handleTags(tagList, countTags);
                    }
                }
            }
        }
        return new ParseResult(changes, countTags);
    }

    private void handleNode(@NotNull Node node, Map<String, Set<String>> changes) {
        String user = node.getUser();
        BigInteger changeSet = node.getChangeset();
        if (user == null || changeSet == null) {
            return;
        }

        changes.putIfAbsent(user, new HashSet<>());
        Set<String> changesValue = changes.get(user);
        changesValue.add(changeSet.toString());
    }

    private void handleTags(@NotNull List<Tag> tagList, Map<String, Integer> countTags) {
        for (Tag tag : tagList) {
            String key = tag.getK();
            if (key != null) {
                countTags.merge(key, 1, Integer::sum);
            }
        }
    }
}
