package com.distributedsystems.lab1;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jetbrains.annotations.NotNull;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.util.*;

public class Parser {

    private final QName nodeQName = new QName("node");
    private final QName tagQName = new QName("tag");
    private final QName userQName = new QName("user");
    private final QName changeSetQName = new QName("changeset");
    private final QName keyQName = new QName("k");

    public ParseResult parse(BZip2CompressorInputStream inputStream) throws XMLStreamException {
        Map<String, Set<String>> changes = new HashMap<>();
        Map<String, Integer> tags = new HashMap<>();
        XMLInputFactory inputFactory = XMLInputFactory.newInstance();
        XMLEventReader reader = inputFactory.createXMLEventReader(inputStream);
        ArrayDeque<StartElement> parentElements = new ArrayDeque<StartElement>();

        while (reader.hasNext()) {
            XMLEvent event = reader.nextEvent();
            if (event.isStartElement()) {
                StartElement element = event.asStartElement();
                if (nodeQName.equals(element.getName())) {
                    handleNode(element, changes);
                }
                if (tagQName.equals(element.getName())) {
                    handleTag(element, parentElements, tags);
                }
                parentElements.addLast(element);
            }
            if (event.isEndElement()) {
                parentElements.removeLast();
            }
        }
        reader.close();
        return new ParseResult(changes, tags);
    }

    private void handleNode(@NotNull StartElement element, Map<String, Set<String>> changes) {
        Attribute user = element.getAttributeByName(userQName);
        Attribute changeSet = element.getAttributeByName(changeSetQName);
        if (user == null || changeSet == null) {
            return;
        }

        String userValue = user.getValue();
        changes.putIfAbsent(userValue, new HashSet<>());
        Set<String> changesValue = changes.get(userValue);
        changesValue.add(changeSet.getValue());
    }

    private void handleTag(@NotNull StartElement element, @NotNull ArrayDeque<StartElement> parentElements, Map<String, Integer> tags) {
        Attribute key = element.getAttributeByName(keyQName);
        StartElement parent = parentElements.getLast();
        if (!parent.getName().equals(nodeQName) || key == null) {
            return;
        }
        String keyValue = key.getValue();
        tags.merge(keyValue, 1, Integer::sum);
    }
}
