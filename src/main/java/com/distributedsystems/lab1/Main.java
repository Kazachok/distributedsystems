package com.distributedsystems.lab1;

import org.apache.commons.cli.*;
import org.apache.commons.compress.compressors.bzip2.BZip2CompressorInputStream;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedInputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;

public class Main {

    public static void main(String[] args) {
        Logger logger = LoggerFactory.getLogger(Main.class);

        Option fileOption = Option.builder("f")
                .hasArg(true)
                .longOpt("file")
                .build();
        Options options = new Options().addOption(fileOption);
        try {
            CommandLine commandLine = new DefaultParser().parse(options, args, true);
            BZip2CompressorInputStream inputStream = new BZip2CompressorInputStream(
                    new BufferedInputStream(
                            Files.newInputStream(
                                    Path.of(commandLine.getOptionValue("file"))
                            )
                    )
            );
            Parser parser = new Parser();
            ParseResult parseResult = parser.parse(inputStream);
            printResult(parseResult);
        } catch (Exception e) {
            logger.error("", e);
        }
    }

    private static void printResult(@NotNull ParseResult result) {
        System.out.println("Количество правок пользователей:");
        Map<String, Set<String>> changes = result.getChanges();
        Map<String, Integer> countChanges = new HashMap<String, Integer>();
        for (Map.Entry<String, Set<String>> change : changes.entrySet()) {
            countChanges.put(change.getKey(), change.getValue().size());
        }
        countChanges.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(entry ->
                        System.out.println(entry.getKey() + " : " + entry.getValue())
                );
        System.out.println();

        Map<String, Integer> tags = result.getTags();
        System.out.println("Количество тегов:");
        for (Map.Entry<String, Integer> tag : tags.entrySet()) {
            System.out.println(tag.getKey() + " : " + tag.getValue());
        }
    }
}
