package com.distributedsystems.lab1;

import java.util.Map;
import java.util.Set;

public class ParseResult {

    private Map<String, Set<String>> changes;
    private Map<String, Integer> tags;

    public ParseResult(Map<String, Set<String>> changes, Map<String, Integer> tags) {
        this.changes = changes;
        this.tags = tags;
    }

    public Map<String, Set<String>> getChanges() {
        return changes;
    }

    public void setChanges(Map<String, Set<String>> changes) {
        this.changes = changes;
    }

    public Map<String, Integer> getTags() {
        return tags;
    }

    public void setTags(Map<String, Integer> tags) {
        this.tags = tags;
    }
}
